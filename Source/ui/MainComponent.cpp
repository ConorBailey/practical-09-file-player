/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& audio_)
: audio (audio_)
{
    for (int i = 0; i < numFilePlayers; i++)
    {
        filePlayerGui[i] = new FilePlayerGui(filePlayers[i]);
        addAndMakeVisible(filePlayerGui[i]);
    }
    
    
    setSize (500, 400);
}

MainComponent::~MainComponent()
{
    for (int i = 0; i < numFilePlayers; i++) {
        delete filePlayerGui[i];
    }
}

void MainComponent::resized()
{
    filePlayerGui[0]->setBounds (0, 0, getWidth()/2, getHeight()/2);
    filePlayerGui[1]->setBounds (filePlayerGui[0]->getBounds().translated(0, getHeight()/2));
    
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

